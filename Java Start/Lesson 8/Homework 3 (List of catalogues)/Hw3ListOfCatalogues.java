package sample;

import java.io.File;

public class Hw3ListOfCatalogues {

	public static void main(String[] args) {

		File workfolder = new File(".");

		System.out.println(listOfCatalogues(workfolder));

	}
	
	public static String listOfCatalogues(File directory) {
		File[] folders = directory.listFiles();
		String text = "";

		for (int i = 0; i < folders.length; i++) {
			if (folders[i].isDirectory())
				text = text + folders[i] + System.lineSeparator();
		}
		return text;
	}
}
