package sample;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class LectionExcercise {

	public static void main(String[] args) {
		File file = new File("Hello.txt");

//	    try (PrintWriter pw = new PrintWriter(file)) {
//
//	      pw.println("Hello world");
//
//	    } catch (IOException e) {
//	      e.printStackTrace();
//	    }

		String text = getStringFromFile(file);

		System.out.println(text);

	}

	public static String getStringFromFile(File file) {
		String text = "";
		try (Scanner sc = new Scanner(file)) {

			for (; sc.hasNextLine();) {
				text = text + sc.nextLine() + System.lineSeparator();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return text.substring(0, text.length() - 1);
	}

}
