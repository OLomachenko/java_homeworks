package sample;

import java.io.File;
import java.io.IOException;

public class Lection {

	public static void main(String[] args) {
		
		File file1 = new File("test.txt");
		
		try {
			boolean t = file1.createNewFile();
			System.out.println(t);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		File folder = new File ("Test");
		
		folder.mkdirs();
		
		File file2 = new File (folder, "Insidetest.docx");
		
		try {
			file2.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		boolean fileDel = file1.delete();
		System.out.println(fileDel);
		

		File workFolder = new File(".");
		File[] files = workFolder.listFiles();
		for (int i = 0; i < files.length; i++) {
			String type = files[i].isDirectory() ? "Folder" : "File";
			long fileSize = files[i].length();
			System.out.println(files[i] + "\t" + type + "\t" + fileSize);
		}
	}
}
