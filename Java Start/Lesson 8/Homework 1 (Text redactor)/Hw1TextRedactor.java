package sample;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Hw1TextRedactor {

	public static void main(String[] args) {
		File redactor = new File("Homework 1 - Text Redactor.txt");
		Scanner sc = new Scanner(System.in);

		System.out.println("Please enter your text:");

		try (PrintWriter pw = new PrintWriter(redactor)) {
			pw.println(sc.nextLine());
		} catch (IOException e) {
			e.printStackTrace();
		}

		sc.close();
	}

}
