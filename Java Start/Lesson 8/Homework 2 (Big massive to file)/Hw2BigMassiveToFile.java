package sample;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

public class Hw2BigMassiveToFile {

	public static void main(String[] args) {
		int[][] doubleMassive ={
				{1, 5, 1, 2, 4, 4, 1, 7},
				{5, 1, 1, 4, 6, 9, 1, 3},
				{8, 8, 4, 1, 9, 2, 3, 3},
				{2, 6, 8, 7, 6, 0, 6, 9},
				{2, 1, 5, 4, 0, 5, 6, 3},
				{7, 1, 4, 9, 6, 3, 8, 9},
				{9, 0, 2, 4, 9, 7, 4, 6},
				{6, 3, 7, 7, 9, 3, 8, 9}};
		
		File massive = new File("Homework 2 - Double Massive.txt");
		String text = intToString(doubleMassive);

		try (PrintWriter pw = new PrintWriter(massive)) {
			pw.print(text);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static String intToString(int[][] mass) {
		String text = "";
		for (int i = 0; i < mass.length; i++) {
			text = text + Arrays.toString(mass[i]) + System.lineSeparator();
		}
		return text;
	}

}
