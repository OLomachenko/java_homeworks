package lection;

public class Main {

	public static void main(String[] args) {

		Employee employee1 = new Employee("Oleksander", "Ts", 1, "Sci dev");

		System.out.println(employee1.getName());

		System.out.println(employee1.hashCode());

		System.out.println(employee1);

		Person person = employee1;

		System.out.println(person.getClass());

		if (person.getClass().equals(Employee.class)) {
			Employee employee = (Employee) person;
			System.out.println(employee.getId());
		}

	}

}
