package projectAcademy;

import java.util.Comparator;

public class StudentLastNameComparator<T> implements Comparator<T> {

	public int compare(T a, T b) {
		Student stud1 = (Student) a;
		Student stud2 = (Student) b;

		int lastNameCompare = stud1.getLastName().compareTo(stud2.getLastName());

		int nameCompare = stud1.getName().compareTo(stud2.getName());

		return (lastNameCompare == 0) ? nameCompare : lastNameCompare;

	}
}
