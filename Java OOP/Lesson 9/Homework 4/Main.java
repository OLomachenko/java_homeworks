package getOneCola;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		List<String> bigBangTheory = new ArrayList<>();
		bigBangTheory.add("Sheldon");
		bigBangTheory.add("Leonard");
		bigBangTheory.add("Volovitc");
		bigBangTheory.add("Kutrapalli");
		bigBangTheory.add("Penny");
		System.out.println(bigBangTheory);

		Scanner sc = new Scanner(System.in);
		System.out.println("How many cans of cola do our friends like to drink?");
		int cola = sc.nextInt();

		for (int i = 0; i < cola; i++) {
			getOneCola(bigBangTheory);
		}
		System.out.println(bigBangTheory);
		sc.close();

	}

	private static void getOneCola(List<String> bigBangTheory) {
		String temp = bigBangTheory.get(0);
		bigBangTheory.add(temp);
		bigBangTheory.add(temp);
		bigBangTheory.remove(0);

	}
}