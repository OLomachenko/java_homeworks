package countLetters;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Lettering {
	private final Character[] alphabet = new Character[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
			'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

	public void countLettersFrom(File file) {
		if (!file.isFile()) {
			System.out.println("This file is a directory! Try again!");
		} else {
			List<Character> letters = textFromFile(file);
			int[] counted = new int[alphabet.length];
			int count = 0;

			for (int i = 0; i < alphabet.length; i++) {
				for (int j = 0; j < letters.size(); j++) {
					if (letters.get(j).equals(alphabet[i])) {
						count++;
					}
				}
				counted[i] = count;
				count = 0;
			}

			List<String> sortedLetters = combineAndSort(counted);
			print(sortedLetters);
		}
	}

	private List<Character> textFromFile(File file) {
		String text = "";
		List<Character> letters = new ArrayList<>();

		try (Scanner sc = new Scanner(file)) {
			for (; sc.hasNext();) {
				text = text + sc.nextLine().toLowerCase() + System.lineSeparator();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		for (char ch : text.toCharArray()) {
			letters.add(ch);
		}
		return letters;
	}

	private List<String> combineAndSort(int[] count) {
		int max = 0;
		int step = 0;
		String temp = "";
		List<String> finalResult = new ArrayList<>();
		for (int i = 0; i < count.length; i++) {
			for (int j = count.length - 1; j >= 0; j--) {
				if (count[j] >= max) {
					max = count[j];
					temp = alphabet[j].toString();
					step = j;
				}
			}
			finalResult.add("Letter " + temp + " : " + max + "\ttimes");
			count[step] = -1;
			max = 0;
		}
		return finalResult;
	}

	private void print(List<String> result) {
		for (String text : result) {
			System.out.println(text);
		}
	}
}
