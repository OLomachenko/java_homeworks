package listWorks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

	public static void main(String[] args) {

		List<String> myList = new ArrayList<>();

		myList.add("Hello");
		myList.add("Java");
		myList.add("world!");
		System.out.println(myList);
		myList.add(1, "my");
		System.out.println(myList);
		myList.remove(1);
		System.out.println(myList);

		Collections.sort(myList);

		System.out.println(myList);

		for (String element : myList) {
			System.out.println(element);
		}

	}

}
