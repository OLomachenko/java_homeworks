package sample;

//public class Container<E extends Number> {
public class Container<E> {
		
	private Object element;

	public Container(E element) {
		super();
		this.element = element;
	}

	public Container() {
		super();
	}

	@SuppressWarnings("unchecked")
	public Object getElement() {
		return (E) element;
	}

	public void setElement(E element) {
		this.element = element;
	}

	@Override
	public String toString() {
		return "Container [element=" + element + "]";
	}

}
