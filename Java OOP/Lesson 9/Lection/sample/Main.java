package sample;

public class Main {

	public static void main(String[] args) {

		Container<Integer> a = new Container<>(100);

//		a.setElement("java");

		int sum = 10 + (Integer) a.getElement();

		System.out.println(sum);

		Container<String> b = new Container<>("Hello world!");

		System.out.println(a);
		System.out.println(b);

		Integer[] array1 = new Integer[] { 5, 20, 13, 7 };
		// Обьявление метода
		Integer max1 = getMaxElement(array1);

		System.out.println(max1);

		// Или обьявлять так, если метод не имеет входных данных
//		Main.<Integer>getMaxElement(array1);
		Container<? extends Comparable<?>> c = a;
		
	}

	public static <V extends Comparable<V>> V getMaxElement(V[] array) {
		V max = array[0];
		for (int i = 0; i < array.length; i++) {
			if (max.compareTo(array[i]) < 0) {
				max = array[i];
			}
		}
		return max;
	}
}
