package lection;

public class Main {

	public static void main(String[] args) {

		Thread mainThread = Thread.currentThread();

		System.out.println(mainThread.getName() + " start");

		FactorialTask task1 = new FactorialTask(500);
		FactorialTask task2 = new FactorialTask(500);
		FactorialTask task3 = new FactorialTask(500);

		Thread thread1 = new Thread(task1);
		Thread thread2 = new Thread(task2);
		Thread thread3 = new Thread(task3);

		thread1.start();
		thread2.start();
		thread3.start();

		try {
			thread1.join();
			thread2.join();
			thread3.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Sum 1 = " + task1.getSum());
	    System.out.println("Sum 2 = " + task2.getSum());
	    System.out.println("Sum 3 = " + task3.getSum());
	    
	    thread1.interrupt();
	    thread2.interrupt();
	    thread3.interrupt();

		try {
			Thread.sleep(3);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(mainThread.getName() + " stop");

	}

}
