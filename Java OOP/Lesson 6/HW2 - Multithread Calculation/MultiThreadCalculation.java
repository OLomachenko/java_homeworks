package homework2;

public class MultiThreadCalculation implements Runnable {
	
	private int start;
	private int end; 
	private int [] array;
	private long sum = 0;
	private Thread thr;
	
	public MultiThreadCalculation( int[] array, int start, int end) {
		super();
		this.start = start;
		this.end = end;
		this.array = array;
		thr = new Thread(this);
		thr.start();
	}

	public long getSum() {
		return sum;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public void setEnd(int end) {
		this.end = end;
	}

	public void setArray(int[] array) {
		this.array = array;
	}

	public Thread getThr() {
		return thr;
	}

	@Override
	public void run() {
		for (int i = start; i < end; i++) {
			sum = sum+(array[i]);
		}
		
	}
}
