package homework2;

import java.util.Random;

public class Main {

	public static void main(String[] args) {
		int[] massive = createMassive();

		calculateSumBasic(massive);

		calculateSumMulti(massive);

	}

	public static int[] createMassive() {
		Random rn = new Random();
		long startTime = System.currentTimeMillis();
		int[] massive = new int[1000000000];
		for (int i = 0; i < massive.length; i++) {
			massive[i] = rn.nextInt(100);
		}
		long stopTime = System.currentTimeMillis();
		System.out.println("Creation of massive used time: " + (stopTime - startTime) + "ms.");
		return massive;
	}

	public static void calculateSumBasic(int[] array) {
		long sum = 0;
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		long endTime = System.currentTimeMillis();
		long timeSpend = endTime - startTime;
		System.out.println("Basic algoritm: Sum = " + sum + " Used time: " + timeSpend + "ms");
	}

	public static void calculateSumMulti(int[] array) {
		int numOfThreads = 4;
		MultiThreadCalculation[] thr = new MultiThreadCalculation[numOfThreads];
		int size = array.length / numOfThreads;
		long sum = 0;
		long startTime = System.currentTimeMillis();

		for (int i = 0; i < numOfThreads; i++) {
			int begin = (size * i);
			int end = (i + 1) * size;
			if (array.length - end < size) {
				end = array.length;
			}
			thr[i] = new MultiThreadCalculation(array, begin, end);
		}
		for (int i = 0; i < thr.length; i++) {
			try {
				thr[i].getThr().join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		for (int i = 0; i < thr.length; i++) {
			sum = sum + (thr[i].getSum());
		}
		long endTime = System.currentTimeMillis();

		System.out.println("Multithread algoritm: Sum = " + sum + " Used time: " + (endTime - startTime) + "ms");
	}

}
