package homework3;

import java.util.Arrays;
import java.util.Random;

public class Main {

	public static void main(String[] args) {
		int[] massive1 = createMassive();
		int[] massive2 = massive1.clone();
		int[] massive3 = massive1.clone();
		int[] massive4 = massive1.clone();

		// Сортировка базовым сортировщиком
		long stime1 = System.currentTimeMillis();
		Arrays.sort(massive1);
		long etime1 = System.currentTimeMillis();
		System.out.println("Сортировка базовым сортировщиком (Arrays.sort): " + (etime1 - stime1) + "ms" + System.lineSeparator());

		// Сортировка методом шелла в однопоточном режиме.
		// sortByShell (массив, количество потоков, номер формулы для расчета шага (1 - Knuth, 2 - Shell)
		long stime2 = System.currentTimeMillis();
		sortByShell(massive2, 1, 1);
		long etime2 = System.currentTimeMillis();
		System.out.println("Сортировка методом Шелла в однопоточном режиме: " + (etime2 - stime2) + "ms" + System.lineSeparator());
		
		// Сортировка методом шелла в многопоточном режиме.
		// sortByShell (массив, количество потоков, номер формулы для расчета шага (1 - Knuth, 2 - Shell)
		long stime3 = System.currentTimeMillis();
		sortByShell(massive3, 8, 1);
		long etime3 = System.currentTimeMillis();
		System.out.println("Сортировка методом Шелла в многопоточном режиме: " + (etime3 - stime3) + "ms" + System.lineSeparator());
		
		// Сортировка вставками в многопоточном режиме.
		// sortByInsert (массив, количество потоков)
		long stime4 = System.currentTimeMillis();
		sortByInsert(massive4, 8);
		long etime4 = System.currentTimeMillis();
		System.out.println("Сортировка вставками в многопоточном режиме: " + (etime4 - stime4) + "ms" + System.lineSeparator());
	}

	public static int[] createMassive() {
		long startTime = System.currentTimeMillis();
		Random rn = new Random();
		int[] massive = new int[1000000];
		for (int i = 0; i < massive.length; i++) {
			massive[i] = rn.nextInt(1000);
		}
		long stopTime = System.currentTimeMillis();
		System.out.println("Creation of massive used time: " + (stopTime - startTime) + "ms." + System.lineSeparator());
		return massive;
	}

	public static void sortByShell(int[] array, int numOfThreads, int generator) {
		MultiThreadSort[] thr = new MultiThreadSort[numOfThreads];
		int size = array.length / numOfThreads;
		
		for (int i = 0; i < numOfThreads; i++) {
			int begin = (size * i);
			int end = (i + 1) * size;
			if (array.length - end < size) {
				end = array.length;
			}
			thr[i] = new MultiThreadSort(array, begin, end, generator);
		}
		
		for (int i = 0; i < thr.length; i++) {
			try {
				thr[i].getThr().join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		if (numOfThreads > 1) 
			System.arraycopy(merge(array, thr), 0, array, 0, array.length);
	}
	
	
	public static void sortByInsert(int[] array, int numOfThreads) {
		BasicSort[] thr = new BasicSort [numOfThreads];
		int size = array.length / numOfThreads;
		
		for (int i = 0; i < numOfThreads; i++) {
			int begin = (size * i);
			int end = (i + 1) * size;
			if (array.length - end < size) {
				end = array.length;
			}
			thr[i] = new BasicSort (array, begin, end);
		}
		
		for (int i = 0; i < thr.length; i++) {
			try {
				thr[i].getThr().join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		if (numOfThreads > 1) 
			System.arraycopy(merge(array, thr), 0, array, 0, array.length);
	}
	
	private static int[] merge (int[] array, MultiThreadSort[] thrs) {
		int [] arr = new int [array.length];
		for (int i = 0; i < arr.length; i++) {
			int temp = Integer.MAX_VALUE;
			int k = -1;
			for (int j = 0; j < thrs.length; j++) {
				if(!thrs[j].isStop() && temp > thrs[j].currentElement()) {
					temp=thrs[j].currentElement();
					k=j;
				}
			}
			if(k!=-1) {
				arr[i] = thrs[k].pollElement();
			}
		}
		return arr;
	}
	
	private static int[] merge (int[] array, BasicSort[] thrs) {
		int [] arr = new int [array.length];
		for (int i = 0; i < arr.length; i++) {
			int temp = Integer.MAX_VALUE;
			int k = -1;
			for (int j = 0; j < thrs.length; j++) {
				if(!thrs[j].isStop() && temp > thrs[j].currentElement()) {
					temp=thrs[j].currentElement();
					k=j;
				}
			}
			if(k!=-1) {
				arr[i] = thrs[k].pollElement();
			}
		}
		return arr;
	}

}
