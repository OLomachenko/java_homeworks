package homework3;

interface StepGenerator {
	public int nextStep();
}

public class MultiThreadSort implements Runnable {

	private int start;
	private int end;
	private int[] array;
	private Thread thr;
	private int index;
	private boolean stop = false;
	int generator;

	public MultiThreadSort(int[] array, int start, int end, int generator) {
		super();
		this.start = start;
		this.end = end;
		this.array = array;
		this.index = start;
		this.generator = generator;
		thr = new Thread(this);
		thr.start();
	}

	public int currentElement() {
		return array[index];
	}

	public int pollElement() {
		int temp = array[index];
		check();
		return temp;
	}

	public boolean isStop() {
		return stop;
	}

	public Thread getThr() {
		return thr;
	}

	public int[] gettArray() {
		return array;
	}

	@Override
	public void run() {
		StepGenerator stepCalc = null;
		if (generator == 1) {
			stepCalc = new KnuthStep(array);
		} else {
			stepCalc = new ShellStep(array);
		}

		int step = stepCalc.nextStep();
		while (step > 0) {
			for (int i = start + step; i < end; i++) {
				for (int j = i; (j - step) >= start && array[j] < array[j - step]; j -= step) {
					int temp = array[j];
					array[j] = array[j - step];
					array[j - step] = temp;
				}
			}
			step = stepCalc.nextStep();
		}
	}

	private void check() {
		this.index++;
		if (this.index >= this.end)
			this.stop = true;
	}
}

class ShellStep implements StepGenerator {
	private int step;

	public ShellStep(int[] array) {
		step = array.length / 2;
	}

	@Override
	public int nextStep() {
		step = step / 2;
		return step;
	}

}

class KnuthStep implements StepGenerator {
	private int i;

	public KnuthStep(int[] array) {
		for (; (Math.pow(3, i) - 1) / 2 < array.length / 3;) {
			i = i + 1;
		}
	}

	@Override
	public int nextStep() {
		int step = (int) ((Math.pow(3, i) - 1) / 2);
		i = i - 1;
		return step;
	}
}
