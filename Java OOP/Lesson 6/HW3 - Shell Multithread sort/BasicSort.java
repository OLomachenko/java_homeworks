package homework3;

public class BasicSort implements Runnable {

	private int start;
	private int end;
	private int[] array;
	private Thread thr;
	private int index;
	private boolean stop = false;

	public BasicSort(int[] array, int start, int end) {
		super();
		this.start = start;
		this.end = end;
		this.array = array;
		thr = new Thread(this);
		thr.start();
		this.index = start;
	}

	public int currentElement() {
		return array[index];
	}

	public int pollElement() {
		int temp = array[index];
		check();
		return temp;
	}

	public boolean isStop() {
		return stop;
	}

	public Thread getThr() {
		return thr;
	}

	public int[] gettArray() {
		return array;
	}

	@Override
	public void run() {
		int temp;
		for (int i = start; i < end; i++) {
			int k = i - 1;
			temp = array[i];
			for (; k >= start && array[k] > temp;) {
				array[k + 1] = array[k];
				array[k] = temp;
				k--;
			}
		}
	}

	private void check() {
		this.index++;
		if (this.index >= this.end)
			this.stop = true;
	}
}
