package multiThreadCopy2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyingThread implements Runnable {

	private Thread thr;
	private File folderIn;
	private File folderOut;
	private CopyService cs;

	public CopyingThread(File in, File out, CopyService cs) {
		super();
		this.folderIn = in;
		this.folderOut = out;
		this.cs = cs;
		thr = new Thread(this);
		thr.start();
	}

	public Thread getThr() {
		return thr;
	}

	@Override
	public void run() {
		try (FileInputStream fis = new FileInputStream(folderIn);
				FileOutputStream fos = new FileOutputStream(folderOut)) {
			fis.transferTo(fos);
//			System.out.println("File: \"" + folderIn.getName() + "\" is copied by " + "Thread-" + thr.getId());
		} catch (IOException e) {
			System.out.println(e);
		}
		cs.workDone();
	}

}
