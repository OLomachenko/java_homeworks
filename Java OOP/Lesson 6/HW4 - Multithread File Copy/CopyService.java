package multiThreadCopy2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

public class CopyService {

	private File[] listOfFiles;
	private int atWork;
	private static int countMulty;
	private static int countSingle;

	public static int getCountMulty() {
		return countMulty;
	}

	public static int getCountSingle() {
		return countSingle;
	}

	private boolean getListOfFiles(File in, File out) {
		if (in.isDirectory()) {
			listOfFiles = in.listFiles();
			if (listOfFiles.length > 0) {
				out.mkdirs();
				return true;
			} else
				System.out.println("Nothing to copy");
			return false;
		} else {
			System.out.format("The directory '%s' does not exist", in);
			return false;
		}
	}

	public void copy(File in, File out, int thrNos) {
		if (getListOfFiles(in, out)) {
			CopyingThread[] thrs = new CopyingThread[listOfFiles.length];
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					File copy = new File(out, listOfFiles[i].getName());
					while(atWork > thrNos) {
						try {
							Thread.sleep(10);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					thrs[i] = new CopyingThread(listOfFiles[i], copy, this);
					atWork++;
					countMulty++;
					
				} else {
					CopyService folder2 = new CopyService();
					File folderOutNew = new File(out, listOfFiles[i].getName());
					folder2.copy(listOfFiles[i], folderOutNew, thrNos);
				}
			}
			for (int i = 0; i < thrs.length; i++) {
				if (!Objects.isNull(thrs[i])) {
					try {
						thrs[i].getThr().join();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	public void oneThreadCopy(File in, File out) {
		if (getListOfFiles(in, out)) {
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					File copy = new File(out, listOfFiles[i].getName());
					copyFile(listOfFiles[i], copy);
					countSingle++;
				} else {
					CopyService folder2 = new CopyService();
					File folderOutNew = new File(out, listOfFiles[i].getName());
					folder2.oneThreadCopy(listOfFiles[i], folderOutNew);
				}

			}

		}

	}

	private void copyFile(File in, File out) {
		try (FileInputStream fis = new FileInputStream(in); FileOutputStream fos = new FileOutputStream(out)) {
			fis.transferTo(fos);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	synchronized public void workDone() {
		atWork--;
	}
}
