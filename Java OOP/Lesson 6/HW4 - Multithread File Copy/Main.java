package multiThreadCopy2;

import java.io.File;

public class Main {

	public static void main(String[] args) {
		File folderOut = new File("D://FolderOut");
		File folderIn1 = new File("D://FolderIn1");
		File folderIn2 = new File("D://FolderIn2");
		int threads = Runtime.getRuntime().availableProcessors();
		CopyService folder1 = new CopyService();
		CopyService folder2 = new CopyService();

		System.out.println("\t--- Multithread Copying ---");
		long stime1 = System.currentTimeMillis();
		folder1.copy(folderOut, folderIn1, threads);
		long etime1 = System.currentTimeMillis();
		System.out.println(CopyService.getCountMulty() + " files are copied. Time spent: " + (etime1 - stime1) + "ms");

		System.out.println(System.lineSeparator() + "\t--- Basic Copying ---");
		long stime2 = System.currentTimeMillis();
		folder2.oneThreadCopy(folderOut, folderIn2);
		long etime2 = System.currentTimeMillis();
		System.out.println(CopyService.getCountSingle() + " files are copied. Time spent: " + (etime2 - stime2) + "ms");
	}

}
