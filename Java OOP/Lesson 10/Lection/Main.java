package lection;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Main {

	public static void main(String[] args) {
		Map<String, String> map = new HashMap<>();

		map.put("j", "Java");
		map.put("t", "the");
		map.put("b", "best");

		map.put("j", "newJava");

		System.out.println(map);

		System.out.println(map.get("j"));

		Set<String> keySet = map.keySet();
		for (String k : keySet) {
			System.out.println(k + " = " + map.get(k));
		}
		System.out.println();

		for (String k : map.keySet()) {
			System.out.println(k + " = " + map.get(k));
		}

		map.remove("t");
		System.out.println("\n" + map);
	}

//	public static void main(String[] args) {
//		List<Integer> list = new ArrayList<>();
//
//		for (int i = 0; i < 15; i++) {
//			list.add((int) (Math.random() * 10));
//		}
//		System.out.println(list);
//
//		Map<Integer, Integer> stat = new HashMap<>();
//		
//		for (Integer element : list) {
//			Integer n = stat.get(element);
//			if (n == null) {
//				stat.put(element, 1);
//			} else {
//				stat.put(element, n + 1);
//			}
//		}
//		System.out.println(stat);
//	}

}
