package sample;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Translator {
	private Map<String, String> dictionary = new HashMap<>();
	private File dict = new File("HW-Translator/Dictionary.txt");

	public Translator() {
		super();
		try (Scanner sc = new Scanner(dict)) {
			while (sc.hasNextLine()) {
				String[] temp = sc.nextLine().split("[-]");
				dictionary.put(temp[0], temp[1]);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}

	public Map<String, String> getDictionry() {
		return dictionary;
	}

	public void translate(File file) {
		String[] textToTranslate = getTextFromFile(file);
		String tText = "";

		for (String text : textToTranslate) {
			if (text != null && dictionary.get(text) != null) {
				tText += dictionary.get(text) + " ";
			}
		}
		String redactedText = tText.substring(0, 1).toUpperCase() + tText.substring(1);

		System.out.println(redactedText);
		writeTextToFile(redactedText);

	}

	public String[] getTextFromFile(File file) {
		String[] text = new String[] {};
		try (Scanner sc = new Scanner(file)) {
			while (sc.hasNextLine()) {
				text = sc.nextLine().toLowerCase().split("[\s-,.]");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return text;
	}

	private void writeTextToFile(String text) {
		File out = new File("HW-Translator/Ukrainian.out");
		out.mkdirs();
		try (FileWriter os = new FileWriter(out)) {
			os.write(text);
		} catch (IOException e) {
			e.getMessage();
		}
	}

	public void updateDictionary() {
		Scanner sc = new Scanner(System.in);
		for (;;) {
			System.out.println("Do you like to update dictionary [y] or not [n]?");
			String result = sc.nextLine();
//			System.out.println();

			if (result.equals("y")) {
				System.out.println("Enter new pair to update dictionary: ");
				System.out.println("Example: \"country-страна\"");
				String importText = sc.nextLine();
				System.out.println();
				String[] convert = importText.split("[-]");
				if (checkDictionary(convert)) {
					writeToDictionary(importText);
					dictionary.put(convert[0], convert[1]);
				}
				continue;
			} else if (result.equals("n")) {
				sc.close();
				break;
			} else {
				System.out.println("Wrong input! Try Again! +\n");
			}
		}
	}

	private void writeToDictionary(String text) {
		try (FileWriter fw = new FileWriter(dict, true)) {
			fw.write(System.lineSeparator() + text);
		} catch (IOException e) {
			e.getMessage();
		}
	}

	private boolean checkDictionary(String[] text) {
		if (dictionary.containsKey(text[0])) {
			System.out.println("This pair already exist \n");
			return false;
		}
		System.out.println("Pair: \"" + text[0] + "-" + text[1] + "\" successfully added to dictionary!");
		return true;
	}

}
