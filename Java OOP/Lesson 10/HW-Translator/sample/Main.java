package sample;

import java.io.File;

public class Main {

	public static void main(String[] args) {
		Translator please = new Translator();
		File importedText = new File("HW-Translator/English.in");
		
		please.updateDictionary();
		
		please.translate(importedText);
	}

}
