package lection;

import java.util.Comparator;

public class NameLengthComparator implements Comparator {
	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		Cat cat1 = (Cat) o1;
		Cat cat2 = (Cat) o2;

		int name1 = cat1.getName().length();
		int name2 = cat2.getName().length();

		if (name1 > name2) {
			return 1;
		}
		if (name1 < name2) {
			return -1;
		}
		return 0;
	}
}
