package lection;

import java.util.Arrays;
import java.util.Comparator;

public class Main {

	public static void main(String[] args) {
		Cat cat1 = new Cat("Murka", 12);
		Cat cat2 = new Cat("Whiskey", 8);
		Cat cat3 = new Cat("Barsik", 6);
		Cat cat4 = new Cat("Simka", 9);
		Cat cat5 = new Cat("Kuzia", 1);

		Cat[] cats = new Cat[] { cat1, cat2, null, cat3, cat4, cat5 };

		for (int i = 0; i < cats.length; i++) {
			System.out.println(cats[i]);
		}

		Arrays.sort(cats, Comparator.nullsLast(new CatAgeComparator()));

		System.out.println();

		for (int i = 0; i < cats.length; i++) {
			System.out.println(cats[i]);
		}

		System.out.println();

		Arrays.sort(cats, Comparator.nullsLast(new NameLengthComparator()));

		for (int i = 0; i < cats.length; i++) {
			System.out.println(cats[i]);
		}
	}

}
