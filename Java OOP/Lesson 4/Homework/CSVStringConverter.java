package groupOfStudents;

public class CSVStringConverter implements StringConverter {

	@Override
	public String toStringRepresentation(Student student) {
		String result = student.getLastName() + "," + student.getName() + "," + student.getGender() + ","
				+ student.getId() + "," + student.getGroupName();
		return result;
	}

	@Override
	public Student fromStringRepresentation(String str) {
		String[] text = str.split(",");
		Student newStudent = new Student();

		newStudent.setLastName(text[0]);
		newStudent.setName(text[1]);
		newStudent.setGender(Gender.valueOf(text[2]));
		newStudent.setId(Integer.parseInt(text[3]));
		newStudent.setGroupName(text[4]);

		return newStudent;
	}

}
