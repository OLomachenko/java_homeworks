package groupOfStudents;

public class Main {

	public static void main(String[] args) {
		CSVStringConverter converting = new CSVStringConverter();
		AddStudent add = new AddStudent();

		Student a1 = new Student("Oleksandr", "Lomachenko", Gender.MALE, 2520, "Java OOP");
		Student a2 = new Student("Volodymyr", "Zelenskyi", Gender.MALE, 2530, "Java OOP");
		Student a3 = new Student("Anna", "Gordiyenko", Gender.FEMALE, 2540, "Java OOP");
		Student a4 = new Student("Olena", "Lomachenko", Gender.FEMALE, 2521, "Java OOP");
		Student a5 = new Student("Vitaliy", "Klychko", Gender.MALE, 2531, "Java OOP");
		Student b1 = new Student("Anastasiya", "Ivanova", Gender.FEMALE, 2541, "Java Start");
		Student b2 = new Student("Andriy", "Zadorojnyi", Gender.MALE, 2522, "Java Start");
		Student b3 = new Student("Mykhaylo", "Safronov", Gender.MALE, 2532, "Java Start");
		Student b4 = add.newStudent();

		Group javaOOP = new Group();

		try {
			javaOOP.addStudent(a1);
			javaOOP.addStudent(a2);
			javaOOP.addStudent(a3);
			javaOOP.addStudent(a4);
			javaOOP.addStudent(a5);
			javaOOP.addStudent(b1);
			javaOOP.addStudent(b2);
			javaOOP.addStudent(b3);
			javaOOP.addStudent(b4);
		} catch (GroupOverflowException e) {

		}

		String text = converting.toStringRepresentation(b4);

		System.out.println(text);

		Student alex = converting.fromStringRepresentation(text);

		System.out.println(alex);

		System.out.println();

		System.out.println(javaOOP);

	}

}
