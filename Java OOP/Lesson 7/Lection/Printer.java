package lection;

public class Printer {
	private Long thrId = null;
	
	synchronized public void printText (String text) {
		long temp = Thread.currentThread().getId();
		for(;thrId != null && thrId == temp;) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println(text);
		thrId = temp;
		notifyAll();
	}

}
