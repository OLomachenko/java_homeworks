package lection;

public class Main {

	public static void main(String[] args) {
		Printer printer = new Printer();

		PrinterTask print1 = new PrinterTask(printer, "push");
		PrinterTask print2 = new PrinterTask(printer, "pull");

		Thread thread1 = new Thread(print1);
		Thread thread2 = new Thread(print2);
		
		thread1.start();
		thread2.start();
	}

}
