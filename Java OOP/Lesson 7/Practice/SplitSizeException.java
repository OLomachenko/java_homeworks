package sample;

public class SplitSizeException extends Exception {

	public SplitSizeException() {
		super();
	}

	public SplitSizeException(String message, Throwable cause) {
		super(message, cause);
	}

	public SplitSizeException(String message) {
		super(message);
	}

	public SplitSizeException(Throwable cause) {
		super(cause);
	}

}
