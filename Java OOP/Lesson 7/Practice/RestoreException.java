package sample;

public class RestoreException extends Exception {

	public RestoreException() {
		super();
	}

	public RestoreException(String message, Throwable cause) {
		super(message, cause);
	}

	public RestoreException(String message) {
		super(message);
	}

	public RestoreException(Throwable cause) {
		super(cause);
	}

}
