package sample;

import java.io.File;
import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		File folderIn = new File("D://FolderOut/TestFile.mp4");

		try {
			FileSplittingService.splitFile(folderIn, 1_048_576);
		} catch (IOException | SplitSizeException e) {
			e.printStackTrace();
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		File dataFile = new File("D:/Temporary/TestFile.mp4_parts/DataFile.txt");

		try {
			boolean result = FileSplittingService.checkFilesForIntegrity(dataFile);
			System.out.println(result);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			System.out.println(FileSplittingService.getFileName(dataFile));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		File restoreFile = new File("D:\\Temporary");

		try {
			FileSplittingService.restoreFile(dataFile, restoreFile);
			System.out.println("Restoration done!");
		} catch (IOException | RestoreException e) {
			e.printStackTrace();
		}

	}

}
