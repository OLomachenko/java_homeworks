package sample;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Arrays;

public class FileSplittingService {

	public static void splitFile(File file, int splitSize) throws IOException, SplitSizeException {
		if (splitSize < 1000 || splitSize > 1_000_000_000) {
			throw new SplitSizeException("Wrong split file size");
		}
		File folderOut = new File("D://Temporary/" + file.getName() + "_parts");
		createPartFolder(folderOut);
		File dataFile = new File(folderOut, "DataFile.txt");

		try (InputStream fis = new FileInputStream(file);
				PrintWriter pw = new PrintWriter(new FileWriter(dataFile, true))) {
			pw.println(file.getName() + ":" + file.length());
			byte[] buffer = new byte[splitSize];
			int readBytes;

			for (int j = 0;; j++) {
				readBytes = fis.read(buffer);
				if (readBytes <= 0) {
					break;
				}
				File part = new File(folderOut, "Part" + j);
				if (readBytes == buffer.length) {
					savePartFile(buffer, part);
				} else {
					byte[] tempBuffer = Arrays.copyOf(buffer, readBytes);
					savePartFile(tempBuffer, part);
				}
				pw.println("Part" + j + ":" + readBytes);

			}

		}

	}

	public static void restoreFile(File dataFile, File folder) throws IOException, RestoreException {
		if (!checkFilesForIntegrity(dataFile)) {
			throw new RestoreException();
		}
		File restoreFile = new File(folder, getFileName(dataFile));
		try (FileOutputStream fos = new FileOutputStream(restoreFile);
				BufferedReader br = new BufferedReader(new FileReader(dataFile))) {
			String fileInfo = br.readLine();
			for (;;) {
				String partInfo = br.readLine();
				if (partInfo == null || partInfo.length() == 0) {
					break;
				}
				String filePartName = partInfo.split("[:]")[0];
				File filePart = new File(dataFile.getParent(), filePartName);
				try (InputStream fis = new FileInputStream(filePart)) {
					fis.transferTo(fos);
				}

			}
		}
	}

	public static void savePartFile(byte[] buffer, File file) throws IOException {
		try (OutputStream os = new FileOutputStream(file)) {
			os.write(buffer);
		}
	}

	public static void createPartFolder(File folder) throws IOException {
		if (folder.exists()) {
			throw new IOException();
		}
		boolean create = folder.mkdirs();
		if (!create) {
			throw new IOException();
		}
	}

	public static boolean checkFilesForIntegrity(File dataFile) throws IOException {
		File folder = new File(dataFile.getParent());
		try (BufferedReader br = new BufferedReader(new FileReader(dataFile))) {
			String fileInfo = br.readLine();
			long fileSize = Long.valueOf(fileInfo.split("[:]")[1]);
			long totalFileSize = 0;
			for (;;) {
				String partInfo = br.readLine();
				if (partInfo == null || fileInfo.length() == 0) {
					break;
				}
				String filePartName = partInfo.split("[:]")[0];
				long filePartSize = Long.valueOf(partInfo.split("[:]")[1]);
				File file = new File(folder, filePartName);
				if (!file.exists() || file.length() != filePartSize) {
					return false;
				} else {
					totalFileSize += filePartSize;
				}
			}
			if (totalFileSize != fileSize) {
				return false;
			}
		}
		return true;
	}

	public static String getFileName(File dataFile) throws IOException {
		try (BufferedReader br = new BufferedReader(new FileReader(dataFile))) {
			return br.readLine().split("[:]")[0];
		}
	}
}
