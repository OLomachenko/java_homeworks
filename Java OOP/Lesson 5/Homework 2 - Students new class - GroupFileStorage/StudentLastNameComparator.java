package groupOfStudents;

import java.util.Comparator;

public class StudentLastNameComparator implements Comparator {
	
	public int compare (Object a, Object b) {
		Student stud1 = (Student) a;
		Student stud2 = (Student) b;
		
		String lastName1 = stud1.getLastName();
		String lastName2 = stud2.getLastName();
		
		return lastName1.compareTo(lastName2);
		
	}
}
