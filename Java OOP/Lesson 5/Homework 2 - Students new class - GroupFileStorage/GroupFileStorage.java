package groupOfStudents;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class GroupFileStorage {

	public void saveGroupToCSV(Group gr) {
		File group = new File("D:/Google Drive/Java/Eclipse for Java/Project Academy/" + gr.getGroupName() + ".csv");
		Student[] students = gr.getStudents();
		String result = "";

		try (PrintWriter pw = new PrintWriter(group)) {
			for (int i = 0; i < students.length; i++) {
				if (students[i] != null) {
					result += StringConverter.toStringRepresentation(students[i]) + System.lineSeparator();
				}
			}
			pw.print(result);
		} catch (FileNotFoundException e) {

		}

	}

	public Group loadGroupFromCSV(File file) {
		Group st = new Group(file.getName().substring(0, file.getName().indexOf(".")));
		try(Scanner sc = new Scanner(file) {
			while (sc.hasNextLine()) {
				st.addStudent(StringConverter.fromStringRepresentation(sc.nextLine()));
			}
		} catch (FileNotFoundException e) {
		} catch (GroupOverflowException e) {
		}
		return st;
	}

	public File findFileByGroupName(String groupName, File workFolder) {
		if (workFolder.isDirectory()) {
			File[] files = workFolder.listFiles();

			for (int i = 0; i < files.length; i++) {
				if (files[i].getName().contains(groupName)) {
					return files[i].getAbsoluteFile();
				}
			}
		}
		return null;

	}
}
