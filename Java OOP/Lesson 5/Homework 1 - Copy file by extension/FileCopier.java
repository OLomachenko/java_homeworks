package copyByExtension;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileCopier {
	
	public static long copyFile (File fileIn, File fileOut) throws IOException{
		try (InputStream is = new FileInputStream(fileIn);
				OutputStream os = new FileOutputStream(fileOut)){
			return is.transferTo(os);
		}
	}
	
	public static int copyFileByExtension(File folderIn, File folderOut, String end) throws IOException {
		File [] folder = folderIn.listFiles();
		int copyFiles = 0;
		
		for (int i = 0; i < folder.length; i++) {
			if (folder[i].isFile() && folder[i].getName().endsWith(end)) {
				File fileOut = new File (folderOut, folder[i].getName());
				copyFile(folder[i], fileOut);
				copyFiles+=1;
			}
		}
		return copyFiles;
		
	}

}
