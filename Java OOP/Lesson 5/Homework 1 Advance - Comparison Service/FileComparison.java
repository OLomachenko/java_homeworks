package fileComparison;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

public class FileComparison {

	public static void isEqual(File file1, File file2) {
		try (FileInputStream is1 = new FileInputStream(file1); FileInputStream is2 = new FileInputStream(file2)) {

			byte[] bytes1 = new byte[10000000];
			byte[] bytes2 = new byte[10000000];
			boolean result = false;
			int pos1 = 0;
			int pos2 = 0;

			for (;;) {

				pos1 = is1.read(bytes1);
				pos2 = is2.read(bytes2);

				if (pos1 <= 0 || pos2 <= 0) {
					break;
				}
				result = Arrays.equals(bytes1, bytes2);
				if (!result)
					break;
			}
			if (result)
				System.out.println("Files are equal");
			else
				System.out.println("Files are NOT equal");

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
