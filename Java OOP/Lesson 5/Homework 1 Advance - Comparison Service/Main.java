package fileComparison;

import java.io.File;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Please enter location of the first file to compare:");
		File file1 = new File(sc.nextLine());
		System.out.println(System.lineSeparator() + "Please enter location of the second file to compare:");
		File file2 = new File(sc.nextLine());

		FileComparison.isEqual(file1, file2);
		sc.close();

	}
}
