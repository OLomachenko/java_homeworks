package lection;

import java.lang.reflect.Field;

public class Main {

	public static void main(String[] args) {
		Cat cat1 = new Cat("Luska", 8);
		Cat cat2 = new Cat("Luska", 8);
		Cat cat3 = new Cat("Luska", 8);

		System.out.println(cat1 == cat2);
		System.out.println(cat1.equals(cat2));
		System.out.println("=============================");
		System.out.println(cat1.equals(cat1));
		System.out.println(cat1.equals(cat2) + "  " + cat2.equals(cat1));
		System.out.println(cat1.equals(cat2) + "  " + cat2.equals(cat3) + "  " + cat1.equals(cat3));
		System.out.println(cat1.equals(cat3) + "  " + (cat1.hashCode() == cat3.hashCode()));
		System.out.println("=============================");

		Cat cat4 = null;

		try {
			cat4 = cat1.clone();

		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}

		System.out.println(cat4 != cat1);
		System.out.println(cat4.getClass() == cat1.getClass());
		System.out.println(cat4.equals(cat1));
		System.out.println(cat4);
		System.out.println("=============================");

//	    Class catClass = cat1.getClass();
		Class catClass = Cat.class;

		Field[] catFields = catClass.getDeclaredFields();
		for (int i = 0; i < catFields.length; i++) {
			System.out.println(catFields[i]);
		}

		try {
			Field catAge = catClass.getDeclaredField("age");
			catAge.setAccessible(true);
			catAge.setInt(cat1, 100500);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}

		System.out.println(cat1);
	}

}
