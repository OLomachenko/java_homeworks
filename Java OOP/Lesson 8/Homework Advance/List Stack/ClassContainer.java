package homeworkAdvance2;

public class ClassContainer {

	public class List {
		Object obj;
		List nextList;

		public List(Object obj, List next) {
			super();
			this.obj = obj;
			this.nextList = next;
		}
	}

	private List list;

	public void push(Object obj) {
		List newList = new List(obj, list);
		list = newList;
	}

	public Object pop() {
		if (list != null) {
			Object result = list.obj;
			list = list.nextList;
			return result;
		}
		return null;
	}

	public Object peek() {
		if (list != null) {
			return list.obj;
		}
		return null;
	}

	@Override
	public String toString() {
		String result = "";
		for (List currentList = list; currentList != null; currentList = currentList.nextList) {
			result += currentList.obj.toString() + System.lineSeparator();
		}
		return result;
	}

}
