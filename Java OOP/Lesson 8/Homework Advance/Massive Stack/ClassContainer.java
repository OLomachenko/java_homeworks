package homeworkAdvance;

import java.util.Arrays;

public class ClassContainer {

	private Object[] array;
	private int size;
	private int capacity;
	private final int default_capacity = 10;
	private final int maxStackSize = Integer.MAX_VALUE - 1;

	public ClassContainer() {
		super();
		array = new Object[default_capacity];
		capacity = array.length;
		size = 0;
	}

	public int getSize() {
		return size;
	}

	public void push(Object obj) {
		if (size >= capacity) {
			boolean resized = increaseSize();
			if (!resized) {
				throw new RuntimeException("Obect cannot be added");
			}
		}
		array[size] = obj;
		size++;
	}

	public Object pop() {
		if (size == 0) {
			return null;
		}
		size -= 1;
		Object result = array[size];
		array[size] = null;
		return result;
	}

	public Object peek() {
		if (size == 0) {
			return null;
		}
		return array[size - 1];
	}

	private boolean increaseSize() {
		if (capacity >= maxStackSize) {
			return false;
		}
		long newCapacity = (long) (array.length * 3 / 2) + 1;
		capacity = (newCapacity >= (long) maxStackSize) ? maxStackSize : (int) newCapacity;
		array = Arrays.copyOf(array, capacity);
		return true;
	}

	@Override
	public String toString() {
		String result = "";
		for (Object obj: array) {
			if(obj != null) {
				result +=obj.toString()+System.lineSeparator();
			}
		}
		return result;
	}

}
