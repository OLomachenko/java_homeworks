package homeworkAdvance;

public class Main {

	public static void main(String[] args) {
		ClassContainer storage = new ClassContainer();

		Human hum1 = new Human("Mihal", "Mihalich", Gender.MALE);
		Human hum2 = new Human("Stepan", "Stepanich", Gender.MALE);
		Student stud1 = new Student(hum1, 2022);
		Student stud2 = new Student(hum2, 2023);

		storage.push(hum1);
		storage.push(hum2);
		storage.push(stud1);
		storage.push(stud2);

		System.out.println(storage);
		System.out.println("================" + System.lineSeparator());

		System.out.println(storage.peek());
		System.out.println(storage.peek());

		System.out.println("================" + System.lineSeparator());

		storage.pop();
		System.out.println(storage.peek());

		System.out.println("================" + System.lineSeparator());

		storage.pop();
		System.out.println(storage);
	}

}
