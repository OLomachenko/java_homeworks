package groupOfStudents;

import java.util.Comparator;

public class StudentLastNameComparator implements Comparator {

	public int compare(Object a, Object b) {
		Student stud1 = (Student) a;
		Student stud2 = (Student) b;

		int lastNameCompare = stud1.getLastName().compareTo(stud2.getLastName());

		int nameCompare = stud1.getName().compareTo(stud2.getName());

		return (lastNameCompare == 0) ? nameCompare : lastNameCompare;

	}
}
