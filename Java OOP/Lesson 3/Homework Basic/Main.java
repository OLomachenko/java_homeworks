package groupOfStudents;

public class Main {

	public static void main(String[] args) {
		Student a1 = new Student("Alexander", "Lomachenko", Gender.MALE, 2520, "Java OOP");
		Student a2 = new Student("Volodymyr", "Zelenskyi", Gender.MALE, 2530, "Java OOP");
		Student a3 = new Student("Anna", "Gordiyenko", Gender.FEMALE, 2540, "Java OOP");
		Student a4 = new Student("Olena", "Lomachenko", Gender.FEMALE, 2521, "Java OOP");
		Student a5 = new Student("Vitaliy", "Klychko", Gender.MALE, 2531, "Java OOP");
		Student b1 = new Student("Anastasiya", "Ivanova", Gender.FEMALE, 2541, "Java Start");
		Student b2 = new Student("Andrey", "Zadorojnyi", Gender.MALE, 2522, "Java Start");
		Student b3 = new Student("Mykhaylo", "Safronov", Gender.MALE, 2532, "Java Start");

		
		Group javaOOP = new Group("Java OOP");
		Group javaStart = new Group("Java Start");
		
		try {
			javaOOP.addStudent(a1);
			javaOOP.addStudent(a2);
			javaOOP.addStudent(a3);
			javaOOP.addStudent(a4);
			javaOOP.addStudent(a5);
			javaStart.addStudent(b1);
			javaStart.addStudent(b2);
			javaStart.addStudent(b3);
		} catch (GroupOverflowException e) {
			e.printStackTrace();
			System.out.println("Group is full");
		}
		System.out.println(javaOOP);
		System.out.println(javaStart);
		
		System.out.println(javaStart.removeStudentById(2522) + System.lineSeparator());
		
		try {
			Student exile = javaOOP.searchStudentByLastName("Lomachenko");
			System.out.println("Student is found:"+System.lineSeparator()+exile);
		} catch (StudentNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println(System.lineSeparator()+"New "+javaStart);
		
	}

}
