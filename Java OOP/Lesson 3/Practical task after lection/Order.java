package practic;

import java.util.Arrays;

public class Order {

	private final Product[] products = new Product[100];

	public Order() {
		super();
	}

	public Product[] getProducts() {
		return products;
	}

	public void addProduct(Product product) {
		for (int i = 0; i < products.length; i++) {
			if (products[i] == null) {
				products[i] = product;
//				System.out.println("Product " + products[i].getDescription() + " added to the order");
				return;
			}
		}
		System.out.println("The order is full! Please create another one");
	}

	public Product searchProductById(int id) throws ProductNotFoundException {
		for (int i = 0; i < products.length; i++) {
			if (products[i] != null) {
				if (products[i].getId() == id) {
					return products[i];
				}
			}

		}
		throw new ProductNotFoundException("Product not found");

	}

	public boolean removeProductById(int id) {
		for (int i = 0; i < products.length; i++) {
			if (products[i] != null) {
				if (products[i].getId() == id) {
					products[i] = null;
					return true;
				}
			}
		}
		return false;
	}

	public int calculateTotalSum() {
		int result = 0;
		for (int i = 0; i < products.length; i++) {
			if (products[i] != null) {
				result += products[i].getPrice();
			}
		}
		return result;
	}

	@Override
	public String toString() {
		String result = "Order:" + System.lineSeparator();
		for (int i = 0; i < products.length; i++) {
			if (products[i] != null) {
				result += products[i] + System.lineSeparator();
			}
		}

		return result + "Total sum: " + calculateTotalSum()+"$";
	}
}
