package practic;

public class Main {

	public static void main(String[] args) {
		Product pen = new Product("pen", 2, 0);
		Product pensil = new Product("pensil", 3, 1);
		Product notebook = new Product("notebook", 10, 2);
		Product diary = new Product("diary", 100, 3);
		
		
		Order monday = new Order();
		
		monday.addProduct(pen);
		monday.addProduct(pensil);
		monday.addProduct(notebook);
		monday.addProduct(diary);
		
		System.out.println(monday);
		
		try {
			Product search = monday.searchProductById(2);
			System.out.println(search);
		} catch (ProductNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println();
		System.out.println(monday.removeProductById(0));
		System.out.println(monday);
		System.out.println();
		System.out.println(monday.removeProductById(10));
		System.out.println(monday);
	}

}
